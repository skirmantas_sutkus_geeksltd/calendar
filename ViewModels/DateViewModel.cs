﻿using System;
using System.Collections.Generic;

namespace Calendar.ViewModels
{
    public class MonthViewModel
    {
        public IEnumerable<CalendarItem> CalendarItems { get; set; }
    }

    public class CalendarItem
    {
        public string Id { get; set; }
        public string Date { get; set; }
        public bool Marked { get; set; }
    }
}
