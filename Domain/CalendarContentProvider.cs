﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Calendar.Controllers;
using Calendar.Model;
using Calendar.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Calendar.Domain
{
    public class CalendarContentProvider
    {
        private readonly CalendarContext _context;

        public CalendarContentProvider(CalendarContext context)
        {
            _context = context;
        }

        public async Task<List<MonthViewModel>> GetCalendarContents(DateTime activeDate, int startModifier, int endModifier)
        {
            var start = new DateTime(activeDate.AddMonths(startModifier).Year, activeDate.AddMonths(startModifier).Month, 1);

            var end = new DateTime(activeDate.AddMonths(endModifier).Year, activeDate.AddMonths(endModifier).Month, DateTime.DaysInMonth(activeDate.AddMonths(endModifier).Year, activeDate.AddMonths(endModifier).Month));

            var range = Enumerable.Range(0, 1 + end.Subtract(start).Days)
              .Select(x => start.AddDays(x))
              .ToArray()
              .GroupBy(x => x.Month);

            var markedDays = await _context.DateMarkers.ToListAsync();

            var result = new List<MonthViewModel>();

            foreach (var month in range)
            {
                var calendarItems = new List<CalendarItem>();

                foreach (var date in month)
                {
                    var dateToAdd = new CalendarItem() { Date = date.ToShortDateString() };

                    var matchingMarker = markedDays.FirstOrDefault(x => x.Date == date);

                    if (matchingMarker != null)
                    {
                        dateToAdd.Id = matchingMarker.Id.ToString();
                        dateToAdd.Marked = true;
                    }

                    else
                    {
                        dateToAdd.Id = Guid.NewGuid().ToString();
                        dateToAdd.Marked = false;
                    }

                    calendarItems.Add(dateToAdd);
                }

                result.Add(new MonthViewModel { CalendarItems = calendarItems });
            }

            return result;
        }
    }
}
