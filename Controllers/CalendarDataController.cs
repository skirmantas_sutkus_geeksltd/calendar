﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Calendar.Model;
using Calendar.ViewModels;
using Microsoft.EntityFrameworkCore;
using Calendar.Domain;

namespace Calendar.Controllers
{
    [ApiController]
    [Route("calendar-data")]
    public class CalendarDataController : ControllerBase
    {
        private readonly CalendarContext _context;

        public CalendarDataController(CalendarContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("get/{activeDate}/{direction}")]
        public async Task<IActionResult> Get(DateTime activeDate, string direction)
        {
            int startModifier;
            int endModifier;

            switch (direction)
            {
                case "before":
                    startModifier = -2;
                    endModifier = 0;
                    break;

                case "around":
                    startModifier = -2;
                    endModifier = 2;
                    break;

                case "after":
                    startModifier = 0;
                    endModifier = 2;
                    break;

                default:
                    return BadRequest("Valid directions are: before, around, after");
            }

            var provider = new CalendarContentProvider(_context);
            return new JsonResult(await provider.GetCalendarContents(activeDate, startModifier, endModifier));
        }

        [HttpPost]
        [Route("toggle/{id}/{date}")]
        public async Task<IActionResult> Toggle(string id, string date)
        {
            var guidId = new Guid(id);

            var dateMarker = await _context.DateMarkers.FirstOrDefaultAsync(x => x.Id == guidId);

            if (dateMarker != null)
                _context.DateMarkers.Remove(dateMarker);

            else
                _context.DateMarkers.Add(new DateMarker
                {
                    Id = guidId,
                    Date = DateTime.Parse(date),
                    DateCreated = DateTime.Now
                });


            await _context.SaveChangesAsync();

            return Ok(id);
        }
    }
}
