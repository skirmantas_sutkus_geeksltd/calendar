import * as React from 'react';
import { Component, FunctionComponent, useState, useEffect } from "react";


class CounterType {
    currentCount: number;

    constructor(_currentCount: number) {
        this.currentCount = _currentCount;
    }
}

const Counteroo: FunctionComponent<{ currentCount?: number }> = ({ currentCount = 0 }) => {
    const [clicks, setClicks] = useState(currentCount);

    useEffect(() => {
        document.title = "Counteroo";
    }, [clicks]);

    return <>
        <div>
            <h1>Counter</h1>

            <p>This is a simple example of a React component.</p>

            <p aria-live="polite">Current count: <strong>{clicks}</strong></p>

            <button className="btn btn-primary" onClick={() => setClicks(clicks + 1)}>Increment</button>
        </div>
    </>
};

export default Counteroo;
