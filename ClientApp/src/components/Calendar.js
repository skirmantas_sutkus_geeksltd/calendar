"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_1 = require("react");
const react_intersection_observer_1 = require("@researchgate/react-intersection-observer");
const $ = require("jquery");
class CalendarViewModel {
    constructor(_loading, _months) {
        this.loading = _loading;
        this.months = [];
        this.activeDate = new Date(new Date().getFullYear(), new Date().getMonth(), 1);
        this.scrollPosition = 0;
        this.isResponsive = isTouchDevice();
        this.originalX = 0;
        this.originalXModified = false;
        this.selectedDayId = "";
        this.selectedDayDate = "";
    }
}
class MonthViewModel {
    constructor(_calendarItems) {
        this.calendarItems = [];
    }
}
class CalendarItem {
    constructor(_id, _date, _marked) {
        this.id = _id;
        this.date = _date;
        this.marked = _marked;
    }
}
const Calendar = ({ initialData = new CalendarViewModel(true) }) => {
    const [loading, setLoading] = react_1.useState(initialData.loading);
    const [months, setMonths] = react_1.useState(initialData.months);
    const [activeDate, setActiveDate] = react_1.useState(initialData.activeDate);
    //const [scrollPosition, setScrollPosition] = useState(initialData.scrollPosition);
    const [isResponsive] = react_1.useState(initialData.isResponsive);
    const testo = async () => {
        var cache = await caches.open("cache1");
        await cache.delete("/calendar-data/get/2020-02-01T00:00:00.000Z/around");
    };
    //function deleteMatchingResponse() {
    //    //calculate which months are in the around
    //    //figure out what would the request for this particular day look like
    //    //remove that request from cache
    //}
    const handleCalendarItemClick = (event) => {
        var _a;
        initialData.selectedDayId = event.currentTarget.id;
        initialData.selectedDayDate = (_a = event.currentTarget.getAttribute("date-attr"), (_a !== null && _a !== void 0 ? _a : ""));
        $(".confirmation-box-wrapper").stop().fadeIn(300);
    };
    const confirmDay = () => {
        var _a;
        var item = (_a = months.map(x => x.calendarItems.find(x => x.id == initialData.selectedDayId))
            .filter(x => x != undefined)[0], (_a !== null && _a !== void 0 ? _a : new CalendarItem("", "", false)));
        $(`.calendar-item#${initialData.selectedDayId}`).toggleClass("marked");
        item.marked = !item.marked;
        testo();
        toggleDate(initialData.selectedDayId, initialData.selectedDayDate);
        setMonths(months);
        $(".confirmation-box-wrapper").stop().fadeOut(300);
    };
    react_1.useEffect(() => {
        document.title = "Calendar";
        fetchJson(`calendar-data/get/${initialData.activeDate.toISOString()}/around`).then((result) => {
            setMonths(result);
            setLoading(false);
            setInitialScroll();
            var wrapper = $(".scroll-wrapper");
            wrapper.on("touchmove", function (e) {
                e.preventDefault();
                if (initialData.originalXModified == false) {
                    initialData.originalXModified = true;
                    initialData.originalX = e.changedTouches[0].screenX;
                }
                var diff = initialData.originalX - e.changedTouches[0].screenX;
                wrapper.get(0).scrollBy(diff, 0);
                initialData.originalX = e.changedTouches[0].screenX;
            });
            wrapper.on("touchend", function (e) {
                initialData.originalXModified = false;
            });
        });
        window.addEventListener("resize", handleResize);
    }, []);
    react_1.useEffect(() => {
        initialData.activeDate = activeDate;
        snapToActive();
    }, [months]);
    function handleFlick() {
        var newPos = getNewPos();
        var scrollWrap = document.getElementsByClassName("scroll-wrapper");
        if (scrollWrap.length > 0) {
            $(".scroll-wrapper").stop().animate({ scrollLeft: newPos }, 250, function () {
                addPrev();
                addNext();
            });
        }
    }
    function addPrev() {
        var dateClone = new Date(initialData.activeDate.getTime());
        var theMonthBeforeThat = new Date(dateClone.setMonth(dateClone.getMonth() - 2));
        var theMonthBeforeThatString = theMonthBeforeThat.toLocaleDateString("en-GB");
        if (!months.some(x => x.calendarItems.some(y => y.date == theMonthBeforeThatString))) {
            fetchJson(`calendar-data/get/${theMonthBeforeThat.toISOString()}/before`).then((result) => {
                setActiveDate(initialData.activeDate);
                setMonths(result.concat(months));
            });
        }
    }
    function addNext() {
        var dateClone = new Date(initialData.activeDate.getTime());
        var theMonthAfterThat = new Date(dateClone.setMonth(dateClone.getMonth() + 2));
        var theMonthAfterThatString = theMonthAfterThat.toLocaleDateString("en-GB");
        if (!months.some(x => x.calendarItems.some(y => y.date == theMonthAfterThatString))) {
            fetchJson(`calendar-data/get/${theMonthAfterThat.toISOString()}/after`).then((result) => {
                setActiveDate(initialData.activeDate);
                setMonths(months.concat(result));
            });
        }
    }
    function getNewPos() {
        var _a, _b, _c;
        var wrapper = $(`.calendar-wrapper[id*='${initialData.activeDate.toLocaleDateString("en-GB")}']`);
        var test = document.getElementById(initialData.activeDate.toLocaleDateString("en-GB"));
        var offset = (_b = (_a = test) === null || _a === void 0 ? void 0 : _a.offsetLeft, (_b !== null && _b !== void 0 ? _b : 0));
        var windowWitdh = window.innerWidth;
        var wrapperWidth = (_c = wrapper.innerWidth(), (_c !== null && _c !== void 0 ? _c : 0));
        var padding = (windowWitdh - wrapperWidth) / 2;
        return offset - padding;
    }
    function handleResize() {
        var _a, _b;
        var containerWidth = (_b = (_a = $(".container").width()) === null || _a === void 0 ? void 0 : _a.toString(), (_b !== null && _b !== void 0 ? _b : "0"));
        $(".calendar-wrapper").css("width", containerWidth);
        setInitialScroll();
    }
    //function goNext() {
    //    var firstOfActiveDate = new Date(Date.UTC(activeDate.getFullYear(), activeDate.getMonth(), 1));
    //    var newDate = new Date(firstOfActiveDate.setMonth(firstOfActiveDate.getMonth() + 1));
    //    var theMonthAfterThat = new Date(newDate.setMonth(newDate.getMonth() + 2));
    //    var theMonthAfterThatString = theMonthAfterThat.toLocaleDateString("en-GB");
    //    if (!months.some(x => x.calendarItems.some(y => y.date == theMonthAfterThatString))) {
    //        fetchJson(`calendar-data/get/${theMonthAfterThat.toISOString()}/after`).then((result) => {
    //            setMonths(months.concat(result));
    //        });
    //    }
    //    setActiveDate(new Date(newDate.setMonth(newDate.getMonth() - 1)));
    //    var containerWidth = $(".container").width() ?? 0;
    //    setScrollPosition(scrollPosition + containerWidth);
    //}
    //function goPrev() {
    //    var firstOfActiveDate = new Date(Date.UTC(activeDate.getFullYear(), activeDate.getMonth(), 1));
    //    var newDate = new Date(firstOfActiveDate.setMonth(firstOfActiveDate.getMonth() - 1));
    //    var theMonthBeforeThat = new Date(newDate.setMonth(newDate.getMonth() - 1));
    //    var theMonthBeforeThatString = theMonthBeforeThat.toLocaleDateString("en-GB");
    //    var containerWidth = $(".container").width() ?? 0;
    //    if (!months.some(x => x.calendarItems.some(y => y.date == theMonthBeforeThatString))) {
    //        fetchJson(`calendar-data/get/${theMonthBeforeThat.toISOString()}/before`).then((result) => {
    //            setMonths(result.concat(months));
    //            setScrollPosition(scrollPosition + containerWidth * 2);
    //        });
    //    }
    //    else setScrollPosition(scrollPosition - containerWidth);
    //    setActiveDate(new Date(newDate.setMonth(newDate.getMonth() + 1)));
    //}
    function setInitialScroll() {
        var _a;
        if ($(".calendar-wrapper") != undefined) {
            var calendarWrapperWidth = (_a = $(".calendar-wrapper").width(), (_a !== null && _a !== void 0 ? _a : 0));
            var wrapper = $(".scroll-wrapper");
            wrapper.scrollLeft(calendarWrapperWidth * 2);
            initialData.activeDate = new Date(Date.UTC(initialData.activeDate.getFullYear(), initialData.activeDate.getMonth(), 1));
        }
    }
    function snapToActive() {
        var newPos = getNewPos();
        $(".scroll-wrapper").scrollLeft(newPos);
    }
    let contents = () => {
        return (loading
            ? React.createElement("p", null,
                React.createElement("em", null, "Loading..."))
            : renderCalendarTable());
    };
    return React.createElement(React.Fragment, null,
        React.createElement("div", null,
            React.createElement("h1", { id: "tabelLabel", onClick: testo }, "Calendar"),
            React.createElement("p", null,
                "Active date: ",
                initialData.activeDate.toLocaleDateString("en-GB")),
            React.createElement("p", null,
                "Month count: ",
                months.length),
            isResponsive
                ? React.createElement(React.Fragment, null)
                : React.createElement(React.Fragment, null,
                    React.createElement("button", { className: "btn btn-primary" }, "GoPrev"),
                    React.createElement("button", { className: "btn btn-primary" }, "GoNext")),
            contents(),
            React.createElement("div", { className: "confirmation-box-wrapper", onClick: () => $(".confirmation-box-wrapper").stop().fadeOut(300) },
                React.createElement("div", { className: "confirmation-box", onClick: (e) => e.stopPropagation() },
                    React.createElement("h2", null, "Please Confirm"),
                    React.createElement("button", { onClick: confirmDay }, "Confirm"),
                    React.createElement("button", { onClick: () => $(".confirmation-box-wrapper").stop().fadeOut(300) }, "Cancel")))));
    function handleIntersection(entry) {
        var _a;
        if (entry.intersectionRatio > 0.1) {
            var id = (_a = entry.target.getAttribute("id"), (_a !== null && _a !== void 0 ? _a : ""));
            var newDateArray = id.split("/").map(x => parseInt(x));
            var newDate = new Date(newDateArray[2], newDateArray[1] - 1, newDateArray[0]);
            initialData.activeDate = newDate;
        }
    }
    function renderCalendarTable() {
        var date = initialData.activeDate.toLocaleDateString("en-GB");
        return (React.createElement(React.Fragment, null,
            React.createElement("div", { className: "scroll-wrapper", onTouchEnd: handleFlick, style: { overflowX: isResponsive ? "scroll" : "hidden" } },
                React.createElement("div", { className: "calendar-container", style: { width: "max-content" } }, months.map(month => React.createElement(react_intersection_observer_1.default, Object.assign({ key: month.calendarItems[0].date }, { onChange: handleIntersection, root: ".scroll-wrapper", threshold: 0.1 }),
                    React.createElement("div", { id: month.calendarItems[0].date, key: month.calendarItems[0].date, className: "calendar-wrapper", style: { width: $(".container").width() } },
                        React.createElement("h2", null, `${monthNames[parseInt(month.calendarItems[0].date.split("/")[1]) - 1]} ${month.calendarItems[0].date.split("/")[2]}`),
                        React.createElement("div", { className: "calendar-widget " + (month.calendarItems.some(item => item.date == date) ? "visible" : "visible") }, month != null
                            ? month.calendarItems.map(x => React.createElement("div", { id: x.id, key: x.id, "date-attr": x.date, className: "calendar-item" + (x.marked ? " marked" : ""), onClick: handleCalendarItemClick },
                                React.createElement("div", { className: "calendar-number" }, x.date.split("/")[0])))
                            : React.createElement("tr", null, "E pepega"))))))),
            isResponsive
                ? React.createElement("div", { className: "scroll-blocker" })
                : React.createElement(React.Fragment, null)));
    }
};
const toggleDate = async (id, date) => {
    await $.post(`calendar-data/toggle/${id}/${date.split("/").join("-")}`);
};
const fetchJson = async (url) => {
    const response = await fetch(url, {
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
    });
    var json = await response.json();
    let result = [];
    Object.assign(result, json);
    return result;
};
const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];
function isTouchDevice() {
    var prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
    var mq = function (query) {
        return window.matchMedia(query).matches;
    };
    if (('ontouchstart' in window)) {
        return true;
    }
    // include the 'heartz' as a way to have a non matching MQ to help terminate the join
    // https://git.io/vznFH
    var query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
    return mq(query);
}
exports.default = Calendar;
//# sourceMappingURL=Calendar.js.map