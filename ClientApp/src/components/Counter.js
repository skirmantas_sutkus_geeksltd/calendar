"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_1 = require("react");
var CounterType = /** @class */ (function () {
    function CounterType(_currentCount) {
        this.currentCount = _currentCount;
    }
    return CounterType;
}());
var Counteroo = function (_a) {
    var _b = _a.currentCount, currentCount = _b === void 0 ? 0 : _b;
    var _c = react_1.useState(currentCount), clicks = _c[0], setClicks = _c[1];
    react_1.useEffect(function () {
        document.title = "Counteroo";
    }, [clicks]);
    return React.createElement(React.Fragment, null,
        React.createElement("div", null,
            React.createElement("h1", null, "Counter"),
            React.createElement("p", null, "This is a simple example of a React component."),
            React.createElement("p", { "aria-live": "polite" },
                "Current count: ",
                React.createElement("strong", null, clicks)),
            React.createElement("button", { className: "btn btn-primary", onClick: function () { return setClicks(clicks + 1); } }, "Increment")));
};
exports.default = Counteroo;
//# sourceMappingURL=Counter.js.map