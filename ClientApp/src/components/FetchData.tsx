import * as React from 'react';
import { Component, FunctionComponent, useState, useEffect } from "react";

class FetchDataType {
    forecasts: Forecast[];
    loading: boolean;

    constructor(_loading: boolean, _forecasts?: []) {
        this.loading = _loading;
        this.forecasts = [];
    }
}

class Forecast {
    date: string;
    temperatureC: number;
    temperatureF: number;
    summary: string;

    constructor(_date: string, _temperatureC: number, _temperatureF: number, _summary: string) {
        this.date = _date;
        this.temperatureC = _temperatureC;
        this.temperatureF = _temperatureF;
        this.summary = _summary;
    }
}

const Forecasteroo: FunctionComponent<{ initialData?: FetchDataType }> = ({ initialData = new FetchDataType(true) }) => {
    const [loading, setLoading] = useState(initialData.loading);
    const [forecasts, setForecasts] = useState(initialData.forecasts);


    useEffect(() => {
        document.title = "Forecasteroo";

        fetchJson("weatherforecast").then((result) => {
            setForecasts(result);
            setLoading(false);
        });
    }, []);

    let contents = () => {
        return (
            loading
                ? <p><em>Loading...</em></p>
                : renderForecastsTable(forecasts)
        )
    }

    return <>
        <div>
            <h1 id="tabelLabel" >Weather forecast</h1>
            <p>This component demonstrates fetching data from the server!</p>
            {contents()}
        </div>
    </>
};

export default Forecasteroo;

const fetchJson = async (url: string): Promise<Forecast[]> => {
    const response = await fetch(url);
    var test = await response.json();

    return test;
};

function renderForecastsTable(forecasts: Forecast[]) {
    return (
        <table className='table table-striped' aria-labelledby="tabelLabel">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Temp. (C)</th>
                    <th>Temp. (F)</th>
                    <th>Summary</th>
                </tr>
            </thead>
            <tbody>
                {forecasts.map(forecast =>
                    <tr key={forecast.date}>
                        <td>{forecast.date}</td>
                        <td>{forecast.temperatureC}</td>
                        <td>{forecast.temperatureF}</td>
                        <td>{forecast.summary}</td>
                    </tr>
                )}
            </tbody>
        </table>
    );
}