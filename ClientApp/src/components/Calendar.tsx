﻿import * as React from 'react';
import { Component, FunctionComponent, useState, useEffect } from "react";
import Observer from '@researchgate/react-intersection-observer';

import $ from "jquery"
$();

class CalendarViewModel {
    loading: boolean;
    months: MonthViewModel[];
    activeDate: Date;
    scrollPosition: number;
    isResponsive: boolean;
    originalX: number;
    originalXModified: boolean;
    selectedDayId: string;
    selectedDayDate: string;

    constructor(_loading: boolean, _months?: [], ) {
        this.loading = _loading;
        this.months = [];
        this.activeDate = new Date(new Date().getFullYear(), new Date().getMonth(), 1);
        this.scrollPosition = 0;
        this.isResponsive = isTouchDevice();
        this.originalX = 0;
        this.originalXModified = false;
        this.selectedDayId = "";
        this.selectedDayDate = "";
    }
}

class MonthViewModel {
    calendarItems: CalendarItem[];

    constructor(_calendarItems?: []) {
        this.calendarItems = [];
    }
}

class CalendarItem {
    id: string;
    date: string;
    marked: boolean;

    constructor(_id: string, _date: string, _marked: boolean) {
        this.id = _id;
        this.date = _date;
        this.marked = _marked;
    }
}

const Calendar: FunctionComponent<{ initialData?: CalendarViewModel }> = ({ initialData = new CalendarViewModel(true) }) => {
    const [loading, setLoading] = useState(initialData.loading);
    const [months, setMonths] = useState(initialData.months);
    const [activeDate, setActiveDate] = useState(initialData.activeDate);
    //const [scrollPosition, setScrollPosition] = useState(initialData.scrollPosition);
    const [isResponsive] = useState(initialData.isResponsive);

    const testo = async () => {
        var cache = await caches.open("cache1");

        await cache.delete("/calendar-data/get/2020-02-01T00:00:00.000Z/around");
    }

    //function deleteMatchingResponse() {
    //    //calculate which months are in the around
    //    //figure out what would the request for this particular day look like
    //    //remove that request from cache
    //}

    const handleCalendarItemClick = (event: React.SyntheticEvent) => {
        initialData.selectedDayId = event.currentTarget.id;
        initialData.selectedDayDate = event.currentTarget.getAttribute("date-attr") ?? "";

        $(".confirmation-box-wrapper").stop().fadeIn(300);
    };

    const confirmDay = () => {
        var item = months.map(x => x.calendarItems.find(x => x.id == initialData.selectedDayId))
            .filter(x => x != undefined)[0] ?? new CalendarItem("", "", false);

        $(`.calendar-item#${initialData.selectedDayId}`).toggleClass("marked");

        item.marked = !item.marked;

        testo();

        toggleDate(initialData.selectedDayId, initialData.selectedDayDate);

        setMonths(months);

        $(".confirmation-box-wrapper").stop().fadeOut(300);
    }

    useEffect(() => {
        document.title = "Calendar";

        fetchJson(`calendar-data/get/${initialData.activeDate.toISOString()}/around`).then((result) => {
            setMonths(result);
            setLoading(false);
            setInitialScroll();

            var wrapper = $(".scroll-wrapper");

            wrapper.on("touchmove", function (e) {
                e.preventDefault();

                if (initialData.originalXModified == false) {
                    initialData.originalXModified = true;

                    initialData.originalX = e.changedTouches[0].screenX;
                }

                var diff = initialData.originalX - e.changedTouches[0].screenX

                wrapper.get(0).scrollBy(diff, 0);

                initialData.originalX = e.changedTouches[0].screenX;
            });

            wrapper.on("touchend", function (e) {
                initialData.originalXModified = false;
            });
        });

        window.addEventListener("resize", handleResize);

    }, []);

    useEffect(() => {
        initialData.activeDate = activeDate;

        snapToActive();
    }, [months]);

    function handleFlick() {
        var newPos = getNewPos();

        var scrollWrap = document.getElementsByClassName("scroll-wrapper");
        if (scrollWrap.length > 0) {
            $(".scroll-wrapper").stop().animate({ scrollLeft: newPos }, 250, function () {

                addPrev();
                addNext();
            });
        }
    }

    function addPrev() {
        var dateClone = new Date(initialData.activeDate.getTime());

        var theMonthBeforeThat = new Date(dateClone.setMonth(dateClone.getMonth() - 2));
        var theMonthBeforeThatString = theMonthBeforeThat.toLocaleDateString("en-GB");

        if (!months.some(x => x.calendarItems.some(y => y.date == theMonthBeforeThatString))) {
            fetchJson(`calendar-data/get/${theMonthBeforeThat.toISOString()}/before`).then((result) => {
                setActiveDate(initialData.activeDate);

                setMonths(result.concat(months));
            });
        }
    }

    function addNext() {
        var dateClone = new Date(initialData.activeDate.getTime());

        var theMonthAfterThat = new Date(dateClone.setMonth(dateClone.getMonth() + 2));
        var theMonthAfterThatString = theMonthAfterThat.toLocaleDateString("en-GB");

        if (!months.some(x => x.calendarItems.some(y => y.date == theMonthAfterThatString))) {
            fetchJson(`calendar-data/get/${theMonthAfterThat.toISOString()}/after`).then((result) => {

                setActiveDate(initialData.activeDate);

                setMonths(months.concat(result));
            });
        }
    }

    function getNewPos() {
        var wrapper = $(`.calendar-wrapper[id*='${initialData.activeDate.toLocaleDateString("en-GB")}']`);
        var test = document.getElementById(initialData.activeDate.toLocaleDateString("en-GB"));

        var offset = test?.offsetLeft ?? 0;
        var windowWitdh = window.innerWidth;
        var wrapperWidth = wrapper.innerWidth() ?? 0;

        var padding = (windowWitdh - wrapperWidth) / 2;

        return offset - padding;
    }

    function handleResize() {

        var containerWidth = $(".container").width()?.toString() ?? "0";
        $(".calendar-wrapper").css("width", containerWidth);

        setInitialScroll();
    }

    //function goNext() {
    //    var firstOfActiveDate = new Date(Date.UTC(activeDate.getFullYear(), activeDate.getMonth(), 1));

    //    var newDate = new Date(firstOfActiveDate.setMonth(firstOfActiveDate.getMonth() + 1));

    //    var theMonthAfterThat = new Date(newDate.setMonth(newDate.getMonth() + 2));
    //    var theMonthAfterThatString = theMonthAfterThat.toLocaleDateString("en-GB");

    //    if (!months.some(x => x.calendarItems.some(y => y.date == theMonthAfterThatString))) {
    //        fetchJson(`calendar-data/get/${theMonthAfterThat.toISOString()}/after`).then((result) => {
    //            setMonths(months.concat(result));
    //        });
    //    }

    //    setActiveDate(new Date(newDate.setMonth(newDate.getMonth() - 1)));

    //    var containerWidth = $(".container").width() ?? 0;

    //    setScrollPosition(scrollPosition + containerWidth);
    //}

    //function goPrev() {
    //    var firstOfActiveDate = new Date(Date.UTC(activeDate.getFullYear(), activeDate.getMonth(), 1));

    //    var newDate = new Date(firstOfActiveDate.setMonth(firstOfActiveDate.getMonth() - 1));

    //    var theMonthBeforeThat = new Date(newDate.setMonth(newDate.getMonth() - 1));
    //    var theMonthBeforeThatString = theMonthBeforeThat.toLocaleDateString("en-GB");

    //    var containerWidth = $(".container").width() ?? 0;

    //    if (!months.some(x => x.calendarItems.some(y => y.date == theMonthBeforeThatString))) {
    //        fetchJson(`calendar-data/get/${theMonthBeforeThat.toISOString()}/before`).then((result) => {
    //            setMonths(result.concat(months));

    //            setScrollPosition(scrollPosition + containerWidth * 2);
    //        });
    //    }

    //    else setScrollPosition(scrollPosition - containerWidth);

    //    setActiveDate(new Date(newDate.setMonth(newDate.getMonth() + 1)));
    //}

    function setInitialScroll() {
        if ($(".calendar-wrapper") != undefined) {
            var calendarWrapperWidth = $(".calendar-wrapper").width() ?? 0;

            var wrapper = $(".scroll-wrapper");
            wrapper.scrollLeft(calendarWrapperWidth * 2);

            initialData.activeDate = new Date(Date.UTC(initialData.activeDate.getFullYear(), initialData.activeDate.getMonth(), 1));
        }
    }

    function snapToActive() {
        var newPos = getNewPos();

        $(".scroll-wrapper").scrollLeft(newPos);
    }

    let contents = () => {
        return (
            loading
                ? <p><em>Loading...</em></p>
                : renderCalendarTable()
        )
    }

    return <>
        <div>
            <h1 id="tabelLabel" onClick={testo}>Calendar</h1>
            <p>Active date: {initialData.activeDate.toLocaleDateString("en-GB")}</p>
            <p>Month count: {months.length}</p>


            {isResponsive
                ? <></>
                : <>
                    <button className="btn btn-primary">GoPrev</button>
                    <button className="btn btn-primary">GoNext</button>
                </>}

            {contents()}
            <div
                className="confirmation-box-wrapper"
                onClick={() => $(".confirmation-box-wrapper").stop().fadeOut(300)}>
                <div
                    className="confirmation-box"
                    onClick={(e: React.SyntheticEvent) => e.stopPropagation()}>
                    <h2>Please Confirm</h2>
                    <button onClick={confirmDay}>Confirm</button>
                    <button onClick={() => $(".confirmation-box-wrapper").stop().fadeOut(300)}>Cancel</button>
                </div>
            </div>
        </div>
    </>

    function handleIntersection(entry: IntersectionObserverEntry) {
        if (entry.intersectionRatio > 0.1) {
            var id = entry.target.getAttribute("id") ?? "";
            var newDateArray = id.split("/").map(x => parseInt(x));
            var newDate = new Date(newDateArray[2], newDateArray[1] - 1, newDateArray[0]);

            initialData.activeDate = newDate;
        }
    }

    function renderCalendarTable() {
        var date = initialData.activeDate.toLocaleDateString("en-GB");
        return (
            <>
                <div
                    className="scroll-wrapper"
                    onTouchEnd={handleFlick}
                    style={{ overflowX: isResponsive ? "scroll" : "hidden" }}>
                    <div className="calendar-container" style={{ width: "max-content" }}>
                        {months.map(month =>
                            <Observer key={month.calendarItems[0].date} {...{ onChange: handleIntersection, root: ".scroll-wrapper", threshold: 0.1 }}>
                                <div
                                    id={month.calendarItems[0].date}
                                    key={month.calendarItems[0].date}
                                    className="calendar-wrapper"
                                    style={{ width: $(".container").width() }}>
                                    <h2>{`${monthNames[parseInt(month.calendarItems[0].date.split("/")[1]) - 1]} ${month.calendarItems[0].date.split("/")[2]}`}</h2>
                                    <div
                                        className={"calendar-widget " + (month.calendarItems.some(item => item.date == date) ? "visible" : "visible")}>
                                        {month != null
                                            ? month.calendarItems.map(x =>
                                                <div
                                                    id={x.id}
                                                    key={x.id}
                                                    date-attr={x.date}
                                                    className={"calendar-item" + (x.marked ? " marked" : "")}
                                                    onClick={handleCalendarItemClick}>
                                                    <div className="calendar-number">{x.date.split("/")[0]}</div>
                                                </div>)
                                            : <tr>E pepega</tr>
                                        }
                                    </div>
                                </div>
                            </Observer>
                        )}
                    </div>
                </div>
                {isResponsive
                    ? <div className="scroll-blocker"></div>
                    : <></>
                }
            </>
        );
    }
};

const toggleDate = async (id: string, date: string) => {
    await $.post(`calendar-data/toggle/${id}/${date.split("/").join("-")}`)
}

const fetchJson = async (url: string): Promise<MonthViewModel[]> => {

    const response = await fetch(url, {
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
    })

    var json = await response.json();

    let result: Array<MonthViewModel> = [];
    Object.assign(result, json);

    return result;
};

const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];

function isTouchDevice() {

    var prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');

    var mq = function (query: string) {
        return window.matchMedia(query).matches;
    }

    if (('ontouchstart' in window)) {
        return true;
    }

    // include the 'heartz' as a way to have a non matching MQ to help terminate the join
    // https://git.io/vznFH
    var query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
    return mq(query);

}

export default Calendar;