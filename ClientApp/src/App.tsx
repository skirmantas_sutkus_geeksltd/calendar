import * as React from 'react';
import { Component } from "react";
import { Route } from 'react-router-dom';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import Counteroo from './components/Counter';
import Forecasteroo from './components/FetchData';
import Calendar from './components/Calendar';

import './styles/master.scss'

export default class App extends Component {
    static displayName = App.name;

    render() {
        return (
            <Layout>
                <Route exact path='/' component={Home} title="Home" />
                <Route path='/counter' component={Counteroo} />
                <Route path='/fetch-data' component={Forecasteroo} />
                <Route path='/calendar' component={Calendar} />
            </Layout>
        );
    }
}
