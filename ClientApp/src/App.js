"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_1 = require("react");
const react_router_dom_1 = require("react-router-dom");
const Layout_1 = require("./components/Layout");
const Home_1 = require("./components/Home");
const Counter_1 = require("./components/Counter");
const FetchData_1 = require("./components/FetchData");
const Calendar_1 = require("./components/Calendar");
require("./styles/master.scss");
class App extends react_1.Component {
    render() {
        return (React.createElement(Layout_1.Layout, null,
            React.createElement(react_router_dom_1.Route, { exact: true, path: '/', component: Home_1.Home, title: "Home" }),
            React.createElement(react_router_dom_1.Route, { path: '/counter', component: Counter_1.default }),
            React.createElement(react_router_dom_1.Route, { path: '/fetch-data', component: FetchData_1.default }),
            React.createElement(react_router_dom_1.Route, { path: '/calendar', component: Calendar_1.default })));
    }
}
exports.default = App;
App.displayName = App.name;
//# sourceMappingURL=App.js.map