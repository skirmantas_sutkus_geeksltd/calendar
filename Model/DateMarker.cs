﻿using System;

namespace Calendar.Model
{
    public class DateMarker
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
