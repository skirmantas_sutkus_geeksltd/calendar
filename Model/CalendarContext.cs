﻿using Microsoft.EntityFrameworkCore;

namespace Calendar.Model
{
    public class CalendarContext : DbContext
    {
        public CalendarContext(DbContextOptions<CalendarContext> options) : base(options)
        {
        }

        public DbSet<DateMarker> DateMarkers { get; set; }
    }


}